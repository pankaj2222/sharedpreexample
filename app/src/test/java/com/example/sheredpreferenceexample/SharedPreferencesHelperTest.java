package com.example.sheredpreferenceexample;

import android.content.SharedPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SharedPreferencesHelperTest {

    private static final String TEST_NAME = "test";
    private static final String TEST_EMAIL = "test@gmail.com";
    private static final String TEST_DOB = "18/01/1990";

    private SharedPreferenceEntry mSharedPreferenceEntry;
    private SharedPreferencesHelper mMockSharedPreferencesHelper;

    @Mock
    SharedPreferences mMockSharedPreferences;
    @Mock
    SharedPreferences.Editor mMockEditor;

    @Before
    public  void initMock(){
        mSharedPreferenceEntry = new SharedPreferenceEntry(TEST_NAME,TEST_DOB,TEST_EMAIL);
        mMockSharedPreferencesHelper = createMockSharedPreference();

    }

     @Test
     public void sharedPreferencesHelper_SaveAndReadPersonalInformation(){
         // Save the personal information to SharedPreferences
         boolean success = mMockSharedPreferencesHelper.savePersonalInfo(mSharedPreferenceEntry);
         assertThat("Checking that SharedPreferenceEntry.save... returns true",
                 success, is(true));

 SharedPreferenceEntry savedSharedPreferenceEntry =
                 mMockSharedPreferencesHelper.getPersonalInfo();

         assertThat(mSharedPreferenceEntry.getName(),
                 is(equalTo(savedSharedPreferenceEntry.getName())));

         assertThat(mSharedPreferenceEntry.getDateofbirt(),
                 is(equalTo(savedSharedPreferenceEntry.getDateofbirt())));

         assertThat(mSharedPreferenceEntry.getEmail(),
                 is(equalTo(savedSharedPreferenceEntry.getEmail())));


     }


    private SharedPreferencesHelper createMockSharedPreference() {
        // Mocking reading the SharedPreferences as if mMockSharedPreferences was previously written
        // correctly.
        when(mMockSharedPreferences.getString(eq(SharedPreferencesHelper.KEY_NAME), anyString()))
                .thenReturn(mSharedPreferenceEntry.getName());
        when(mMockSharedPreferences.getString(eq(SharedPreferencesHelper.KEY_EMAIL), anyString()))
                .thenReturn(mSharedPreferenceEntry.getEmail());
        when(mMockSharedPreferences.getString(eq(SharedPreferencesHelper.KEY_DOB), anyString()))
                .thenReturn(mSharedPreferenceEntry.getDateofbirt());
        // Mocking a successful commit.
        when(mMockEditor.commit()).thenReturn(true);
        // Return the MockEditor when requesting it.
        when(mMockSharedPreferences.edit()).thenReturn(mMockEditor);
        return new SharedPreferencesHelper(mMockSharedPreferences);
    }
}