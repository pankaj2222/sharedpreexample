package com.example.sheredpreferenceexample;

public class SharedPreferenceEntry {

    String name;
    String dateofbirt;
    String  email;

    public SharedPreferenceEntry(String name, String dateofbirt, String email) {
        this.name = name;
        this.dateofbirt = dateofbirt;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateofbirt() {
        return dateofbirt;
    }

    public void setDateofbirt(String dateofbirt) {
        this.dateofbirt = dateofbirt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
