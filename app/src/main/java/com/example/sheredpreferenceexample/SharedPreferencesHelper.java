package com.example.sheredpreferenceexample;

import android.content.SharedPreferences;


public class SharedPreferencesHelper {
    static final String KEY_NAME = "key_name";
    static final String KEY_DOB = "key_dob";
    static final String KEY_EMAIL = "key_email";


    // The injected SharedPreferences implementation to use for persistence.
    private final SharedPreferences mSharedPreferences;

    public SharedPreferencesHelper(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
    }

    public boolean savePersonalInfo(SharedPreferenceEntry sharedPreferenceEntry) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(KEY_NAME, sharedPreferenceEntry.getName());
        editor.putString(KEY_DOB, sharedPreferenceEntry.getDateofbirt());
        editor.putString(KEY_EMAIL, sharedPreferenceEntry.getEmail());
        return editor.commit();
    }

    public SharedPreferenceEntry getPersonalInfo() {
        String name = mSharedPreferences.getString(KEY_NAME, "");
        String dob = mSharedPreferences.getString(KEY_DOB, "");
        String email = mSharedPreferences.getString(KEY_EMAIL, "");
        return new SharedPreferenceEntry(name, dob, email);
    }
}
